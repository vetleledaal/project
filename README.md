# IKT446 Project
Demo for an umbrella front-end showcasing MySQL, MongoDB, and Neo4j as swappable backends. The dataset is from [Project Tycho](https://www.tycho.pitt.edu).

SNOMED-CT name: Coccidioidomycosis

SNOMED-CT code: 60826002

## Configuration
Set up variables to be used throughout the project in `.env`. User for MySQL and Neo4j can not be changed at this time.

## Run
```bash
docker-compose up -d
```

Since the Flask application depends on all the databases to be initialized we need to run it afterwards
```bash
docker-compose up -d web
```


## Auxiliary scripts
Scripts need to run within the existing Docker network to access services. We reuse the Python container as it has all the required dependencies. The container name `project_web` may change depending on the current folder.

**Create schema**
```bash
docker run -it --rm -v "$PWD:/root" -w /root/scripts --network="project_default" project_web python mysql_execute.py 01_create_schema.sql
```

**Import data to ODB**
```bash
docker run -it --rm -v "$PWD:/root" -w /root/scripts --network="project_default" project_web python mysql_import_data.py
```

**Populate ADB**
```bash
docker run -it --rm -v "$PWD:/root" -w /root/scripts --network="project_default" project_web python mysql_execute.py 02_populate_adb.sql
docker run -it --rm -v "$PWD:/root" -w /root/scripts --network="project_default" project_web python mysql_populate_adb_cumulative.py
```


## Data enrichment example
```sql
UPDATE `ADB`.`statedim`
SET `name` = 'Ohio'
WHERE `state` = 'OH';

UPDATE `ADB`.`statedim`
SET `name` = 'Wisconsin'
WHERE `state` = 'WI';
```