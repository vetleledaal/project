#!/usr/bin/false
import pandas as pd
from sqlalchemy import create_engine
from dotenv import dotenv_values
from load_data import load_data

def insert_data(db, df):
    engine = create_engine('mysql://%s:%s@mysql:%s/ODB' % (db.get('MYSQL_USER'), db.get('MYSQL_PASS'), db.get('MYSQL_PORT')))
    df.to_sql(name='health_data', con=engine, if_exists='replace')


if __name__ == '__main__':
    config = dotenv_values('../.env')
    df = load_data()
    print(df.head())
    insert_data(config, df)
