import pandas as pd

def load_data():
    df = pd.read_csv(
        '../data/US.60826002.csv',
        header=0,
        names=['state', 'period_start', 'period_end', 'cumulative', 'count'],
        usecols=[8, 11, 12, 13, 19],
    )
    df.replace({r'^US-': ''}, regex=True, inplace=True)
    return df
