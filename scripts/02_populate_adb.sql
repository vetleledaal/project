USE `ADB`;

INSERT INTO `ADB`.`statedim` (`state`)
SELECT DISTINCT `state`
FROM `ODB`.`health_data`;

INSERT INTO `ADB`.`perioddim` (`period_start`, `period_end`)
SELECT DISTINCT `period_start`, `period_end`
FROM `ODB`.`health_data`;

INSERT INTO `ADB`.`datadim` (`cumulative`, `count`)
SELECT DISTINCT `cumulative`, `count`
FROM `ODB`.`health_data`;

INSERT INTO `ADB`.`fact` (`id_state`, `id_period`, `id_data`)
SELECT `statedim`.`id` as `id_state`, `perioddim`.`id` as `id_period`, `datadim`.`id` as `id_data`
FROM `ODB`.`health_data`
INNER JOIN `ADB`.`statedim` ON `ODB`.`health_data`.`state` = `ADB`.`statedim`.`state`
INNER JOIN `ADB`.`perioddim` ON `ODB`.`health_data`.`period_start` = `ADB`.`perioddim`.`period_start` AND `ODB`.`health_data`.`period_end` = `ADB`.`perioddim`.`period_end`
INNER JOIN `ADB`.`datadim` ON `ODB`.`health_data`.`cumulative` = `ADB`.`datadim`.`cumulative` AND `ODB`.`health_data`.`count` = `ADB`.`datadim`.`count`;


SELECT `state`, SUM(`count`) as `count`, `cumulative`
FROM `ADB`.`fact`
INNER JOIN `ADB`.`statedim` ON `ADB`.`fact`.`id_state` = `ADB`.`statedim`.`id`
INNER JOIN `ADB`.`perioddim` ON `ADB`.`fact`.`id_period` = `ADB`.`perioddim`.`id`
INNER JOIN `ADB`.`datadim` ON `ADB`.`fact`.`id_data` = `ADB`.`datadim`.`id`
WHERE `cumulative` = 0
GROUP BY `state`, `cumulative`
ORDER BY `count` DESC;


-- not used
-- handled in Python, as cumulative is hard to handle in SQL
-- USE `ADB2`;
-- INSERT INTO `perioddim` (`state`, `period_start`, `period_length`, `count_per_day`)
-- SELECT `state`, `period_start`, DATEDIFF(`period_end`, `period_start`), `count`/DATEDIFF(`period_end`, `period_start`)
-- WHERE `cumulative` = 0
-- FROM `ADB`.`timedim`;