#!/usr/bin/false
import pandas as pd
import json
from dotenv import dotenv_values
from pymongo import MongoClient
from load_data import load_data

df = load_data()
json_df = df.to_json(orient="records")
json_data = json.loads(json_df)


if __name__ == '__main__':
    config = dotenv_values('../.env')
    client = MongoClient(host='mongo', port=int(config.get('MONGO_PORT')), username=config.get('MONGO_USER'), password=config.get('MONGO_PASS'))
    client["valleyfever"]["data"].insert_many(json_data)
    print(client.list_database_names())

