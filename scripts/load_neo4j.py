import pandas as pd
from datetime import datetime
from neo4j import GraphDatabase



def load_data():
    df = pd.read_csv(
        '../data/US.60826002.csv',
        header=0,
        names=['state', 'period_start', 'period_end', 'cumulative', 'count'],
        usecols=[8, 11, 12, 13, 19],
    )
    df.replace({r'^US-': ''}, regex=True, inplace=True)
    return df

def load_states():
    df = pd.read_csv(
        '../data/US.60826002.csv',
        header=0,
        names=['statename', 'state'],
        usecols=[8, 7],
    )
    df.replace({r'^US-': ''}, regex=True, inplace=True)
    return df

def load_periods():
    df = pd.read_csv(
        '../data/US.60826002.csv',
        header=0,
        names=['period_start', 'period_end'],
        usecols=[11, 12],
    )
    df.replace({r'^US-': ''}, regex=True, inplace=True)
    
    df = df.drop_duplicates()
    
    period_length = []

    for ind in df.index:
         period_length.append((datetime.strptime(df['period_end'][ind], '%Y-%m-%d') - datetime.strptime(df['period_start'][ind], '%Y-%m-%d')).days)
    df['period_length'] = period_length
    
    return df


if __name__ == '__main__':
    df = load_data()
    df2 = load_states()
    df3 = load_periods()
    df2 = df2.drop_duplicates()

    ####### import states into neo4j ##### 
    dataframe_list = df2.values.tolist()

    dataframe_execution_commands = []

    for i in dataframe_list:
        neo4j_create_statemenet = "create (s:State {state:" + "\"" + str(i[1]) +"\", statename:  " + "\"" + str(i[0]) +"\"" + "})"

        dataframe_execution_commands.append(neo4j_create_statemenet)

        
    def execute_transactions(dataframe_execution_commands):
        data_base_connection = GraphDatabase.driver(uri = "bolt://neo4j:7687", auth=("neo4j", "pEaKqm73XQYc")) # 123 = password
        session = data_base_connection.session()    
        for i in dataframe_execution_commands:
            session.run(i)

            
    execute_transactions(dataframe_execution_commands)



    #print(dataframe_execution_commands)
    ####### import time_periods into neo4j ##### 
    dataframe_list = df3.values.tolist()

    dataframe_execution_commands = []

    for i in dataframe_list:
        neo4j_create_statemenet = "create (t:Timeperiod {period_start:" + "\"" + str(i[0]) +"\", period_end:" + "\"" + str(i[1]) +"\", period_length:" + str(i[2]) + "})"

        dataframe_execution_commands.append(neo4j_create_statemenet)

        
    def execute_transactions(dataframe_execution_commands):
        data_base_connection = GraphDatabase.driver(uri = "bolt://neo4j:7687", auth=("neo4j", "pEaKqm73XQYc")) # 123 = password
        session = data_base_connection.session()    
        for i in dataframe_execution_commands:
            session.run(i)

            
    execute_transactions(dataframe_execution_commands)

    ####### import data into neo4j ##### 

    dataframe_list = df.values.tolist()

    dataframe_execution_commands = []

    for i in dataframe_list:
        neo4j_create_statemenet = "create (r:Record {state:" + "\"" + str(i[0]) +"\", period_start:  " + "\"" + str(i[1]) +"\", period_end: " + "\""  + str(i[2]) +"\", cumulative: " + str(i[3]) + ", count: "+ str(i[4]) + "})"

        dataframe_execution_commands.append(neo4j_create_statemenet)

        
    def execute_transactions(dataframe_execution_commands):
        data_base_connection = GraphDatabase.driver(uri = "bolt://neo4j:7687", auth=("neo4j", "pEaKqm73XQYc")) # 123 = password
        session = data_base_connection.session()    
        for i in dataframe_execution_commands:
            session.run(i)


    execute_transactions(dataframe_execution_commands)

    #print(dataframe_execution_commands)

    ###### create relationships #####

    #relationship from record to state
    execute_transactions(["MATCH (a:Record), (b:State) WHERE a.state = b.state CREATE (a)-[r:IsInState]->(b)"])

    #relationship from record to time_period
    execute_transactions(["MATCH (a:Record), (b:Timeperiod) WHERE a.period_start = b.period_start AND a.period_end = b.period_end  CREATE (a)-[r:IsInPeriod]->(b)"])