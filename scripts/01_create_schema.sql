DROP SCHEMA IF EXISTS `ODB`;
CREATE SCHEMA `ODB`;
USE `ODB`;
CREATE TABLE `health_data` (
  `index` INT NOT NULL AUTO_INCREMENT,
  `state` CHAR(3) NOT NULL,
  `period_start` DATE NOT NULL,
  `period_end` DATE NOT NULL,
  `cumulative` TINYINT NOT NULL,
  `count` INT NOT NULL,
  PRIMARY KEY (`index`)
);

DROP SCHEMA IF EXISTS `ADB`;
CREATE SCHEMA `ADB`;
USE `ADB`;
CREATE TABLE `statedim` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `state` CHAR(2) NOT NULL,
  `name` VARCHAR(20),
  `country` CHAR(2) NOT NULL DEFAULT 'US',
  PRIMARY KEY (`id`)
);

CREATE TABLE `perioddim` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `period_start` DATE NOT NULL,
  `period_end` DATE NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `datadim` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `cumulative` TINYINT NOT NULL,
    `count` INT NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `fact` (
  `id` INT NOT NULL,
  `id_state` INT NOT NULL,
  `id_period` INT NOT NULL,
  `id_data` INT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_state`) REFERENCES `statedim` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`id_period`) REFERENCES `perioddim` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`id_data`) REFERENCES `datadim` (`id`) ON DELETE CASCADE
);
