#!/usr/bin/false
import pandas as pd
from sqlalchemy import create_engine
from dotenv import dotenv_values

def load_data_from_db(db):
    return pd.read_sql_query('SELECT * FROM `ADB`.`timedim`', db)

def sort_data(df):
    df['period_start'] = pd.to_datetime(df['period_start'])
    df['period_end'] = pd.to_datetime(df['period_end'])
    return df.sort_values(by=['period_start', 'period_end'])

def transmute_data(df):
    # Turn period_end into period_length
    df.rename(columns={'period_end': 'period_length'}, inplace=True)
    df['period_length'] = df['period_length'] - df['period_start']

    # Make data non-cumulative
    # TODO: Make this work
    # 
    current = {}
    for _, row in df.iterrows():
        s = row['state']

        current[s] = 0
        df['count'] = current[s]
        current[s] -= row['count']
    pd.set_option('display.max_rows', 800)
    print(df.head(n=800))
    exit(1)


def insert_to_db(db, df):
    df.to_sql('timedim', db, if_exists='replace', index=False)

if __name__ == '__main__':
    config = dotenv_values('../.env')
    engine = create_engine('mysql://%s:%s@mysql:%s' % (config.get('MYSQL_USER'), config.get('MYSQL_PASS'), config.get('MYSQL_PORT')))
    df = load_data_from_db(engine)
    df = sort_data(df)
    df = transmute_data(df)
    insert_to_db(engine, df)
    print(df.head())