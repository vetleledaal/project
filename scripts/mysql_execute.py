#!/usr/bin/false
from sqlalchemy import create_engine
from dotenv import dotenv_values
import argparse


if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('file', nargs='+', help='File to execute')
    args = p.parse_args()

    config = dotenv_values('../.env')
    engine = create_engine('mysql://%s:%s@mysql:%s' % (config.get('MYSQL_USER'), config.get('MYSQL_PASS'), config.get('MYSQL_PORT')))
    
    for file in args.file:
        with open(file, 'r') as f:
            sql = f.read()
            engine.execute(sql)
            print('Executed %s' % file)