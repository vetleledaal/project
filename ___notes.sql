CREATE SCHEMA IF NOT EXISTS `ODB`;
USE `ODB`;
CREATE TABLE `health_data` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `state` CHAR(3) NOT NULL,
  `period_start` DATE NOT NULL,
  `period_end` DATE NOT NULL,
  `count` INT NOT NULL,
  `cumulative` TINYINT NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE SCHEMA IF NOT EXISTS `ADB`;
USE `ADB`;
CREATE TABLE `fact` (
  `id` INT NOT NULL,
  `state` CHAR(2) NOT NULL,
  `period_start` DATE NOT NULL,
  `period_end` DATE NOT NULL,
  `count` INT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id`) REFERENCES `ODB`.`health_data` (`id`)
);

CREATE TABLE `timedim` (
  `state` CHAR(2) NOT NULL,
  `period_start` DATE NOT NULL,
  `period_end` DATE NOT NULL,
  `count` INT NOT NULL,
  PRIMARY KEY (`state`, `period_start`, `period_end`)
);

CREATE TABLE `statedim` (
  `state` CHAR(2) NOT NULL,
  `name` VARCHAR(20),
  `country` CHAR(2) NOT NULL DEFAULT 'US',
  PRIMARY KEY (`state`)
);

CREATE SCHEMA IF NOT EXISTS `ADB2`;
USE `ADB2`;
CREATE TABLE `perioddim` (
    `state` CHAR(2) NOT NULL,
    `period_start` DATE NOT NULL,
    `period_length` INT NOT NULL,
    `count_per_day` FLOAT NOT NULL,
    PRIMARY KEY (`state`, `period_start`, `period_length`)
);

CREATE SCHEMA IF NOT EXISTS `ADB3`;
USE `ADB3`;
CREATE TABLE `distributiondim` (
    `state` CHAR(2) NOT NULL,
    `date` DATE NOT NULL,
    `count` INT NOT NULL,
    PRIMARY KEY (`state`, `date`)
);


--------------------------------------------------------------------------------
-- Queries to populate ADB
USE `ADB`;
INSERT INTO `fact` (`id`, `state`, `period_start`, `period_end`, `count`)
SELECT `id`, `state`, `period_start`, `period_end`, `count`
WHERE `cumulative` = 1
FROM `ODB`.`health_data`;


INSERT INTO `timedim` (`state`, `period_start`, `period_end`, `count`)
SELECT `state`, `period_start`, `period_end`, `count`
FROM `fact`;

INSERT INTO `statedim` (`state`)
SELECT DISTINCT `state`
FROM `fact`;

USE `ADB2`;
INSERT INTO `perioddim` (`state`, `period_start`, `period_length`, `count_per_day`)
SELECT `state`, `period_start`, DATEDIFF(`period_end`, `period_start`), `count`/DATEDIFF(`period_end`, `period_start`)
WHERE `cumulative` = 0
FROM `ADB`.`timedim`;

USE `ADB3`;
INSERT INTO `distributiondim` (`state`, `date`, `count`)
SELECT `state`,

GROUP BY (``)

set @i = -1;
SELECT DATE(ADDDATE('2012-02-10', INTERVAL @i:=@i+1 DAY)) AS date FROM `table`
HAVING 
@i < DATEDIFF('2012-02-15', '2012-02-10') 

------

1. Create database with all dates 2000-2020 in first column, count in second column


2. add values to dates with: 

    INSERT INTO database 'count'
    SELECT `count`/DATEDIFF(`period_end`, `period_start`)
    FROM `ADB`.`timedim`
    WHERE OrderDate BETWEEN period_start AND period_end;



SELECT * FROM Orders
WHERE OrderDate BETWEEN 07/01/1996 AND 07/31/1996;




date = 1-365

2017-12-16  -   2018-01-20

December 16 is the 350th day of the year

350 - 20


--------------------------------------------------------------------------------



---- fra time

INSERT INTO ADB.fact
SELECT pid, sid, day, month, year, sum(totalcost)
FROM ODB.transaction
GROUP BY pid, sid, day, month, year;


	
INSERT INTO ADB.product_dim
SELECT pid, pname, pcategory
FROM ODB.prod;


INSERT INTO ADB.locdim
SELECT sid, scity, scountry
FROM ODB.store;


INSERT INTO ADB.timedim
SELECT DISTINCT day, month, year
FROM ODB.transaction;



--------------------------------------------------------------------------------


delete from prod;
delete from store;
delete from customer;
delete from transaction;
insert into prod values (1, 'bread', 35, 'goman', 'food');
insert into prod values (2, 'butter', 40, 'tine', 'food');
insert into prod values (3, 'ipod', 6000, 'apple', 'electronics');
insert into customer values (1, 'John', 'psb');
insert into customer values (2, 'Smith', 'nyc');
insert into store values (1, 'walmart', 'psb', 'pa', 'usa');
insert into store values (2, 'walmart', 'psb', 'pa', 'usa');
insert into store values (3, 'target', 'nyc', 'ny', 'usa');
insert into store values (4, 'target', 'buff', 'ny', 'usa');
insert into transaction values (1, 1, 1, 1, 2, 70, 1, 1, 2021);
insert into transaction values (2, 2, 1, 1, 1, 40, 1, 1, 2021);
insert into transaction values (3, 1, 2, 1, 3, 105, 1, 1, 2021);
insert into transaction values (4, 2, 2, 1, 1, 40, 1, 1, 2021);








PathogenTaxonID	Fatalities	CountryName	CountryISO	Admin1Name	Admin1ISO	Admin2Name	CityName	PeriodStartDate	PeriodEndDate	PartOfCumulativeCountSeries	AgeRange	Subpopulation	PlaceOfAcquisition	DiagnosisCertainty	SourceName	CountValue




