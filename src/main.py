import json
import os
from flask import Flask, render_template, jsonify, request
from flask import redirect, url_for, flash, make_response
import MySQLdb
from bson import json_util
from pymongo import MongoClient
from neo4j import GraphDatabase
from dotenv import dotenv_values


db = {}
app = Flask(__name__)

################################################################################

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET'])
def login():
    r = make_response(redirect(url_for('index')))
    r.set_cookie('session', '1')
    return r

@app.route('/logout', methods=['GET'])
def logout():
    r = make_response(redirect(url_for('index')))
    r.delete_cookie('session')
    return r

@app.route('/api/v1/queryGet', methods=['GET'])
def api_v1_queryGet():
    '''
    Get queries given backend
    '''
    data = request.args
    if db.get(data.get('backend')) is None:
        return jsonify({'error': 'Backend not found'}), 404

    return jsonify(queries[data.get('backend')])

@app.route('/api/v1/queryExecuteCustom', methods=['POST'])
def api_v1_queryExecuteCustom():
    '''
    Execute query given backend, query
    '''
    req_data = request.get_json()
    if db.get(req_data.get('backend')) is None:
        return jsonify({'error': 'Backend not found'}), 404

    if not isAdmin():
        return jsonify({'error': 'Authentication required'}), 401

    if req_data.get('backend') == 'mysql':
        if req_data.get('query') is None:
            return jsonify({'error': 'query required'}), 400

        cursor = db['mysql'].cursor()
        cursor.execute(req_data.get('query'))
        return jsonify({
            'legend': [x[0] for x in cursor.description],
            'data': cursor.fetchmany(1000),
            'count': cursor.rowcount,
        })

    if req_data.get('backend') == 'mongo':
        if not all((req_data.get('database'), req_data.get('collection'), req_data.get('query'))):
            return jsonify({'error': 'database, collection and query required'}), 400

        query = json.loads(req_data.get('query'))
        collection = db['mongo'][req_data.get('database')][req_data.get('collection')]
        cursor = collection.aggregate(query)
        
        obj = unbson(cursor)


        # kill it with fire
        return jsonify({
            'legend': list(obj[0].keys()),
            'data': [list(x.values()) for x in obj],
            'count': len(obj),
        })

    return jsonify({'error': 'Database is valid, but it has not been implemented in the frontend'}), 501

@app.route('/api/v1/queryExecute', methods=['POST'])
def api_v1_execute():
    '''
    Execute query given backend, name
    '''
    req_data = request.get_json()
    if db.get(req_data.get('backend')) is None:
        return jsonify({'error': 'Backend not found'}), 404

    if req_data.get('backend') == 'mysql':
        # largely duplicate code from api_v1_queryExecuteCustom
        if req_data.get('query_name') is None:
            return jsonify({'error': 'query_name required'}), 400

        cursor = db['mysql'].cursor()
        cursor.execute(queries['mysql'][req_data.get('query_name')]['query'])
        return jsonify({
            'legend': [x[0] for x in cursor.description],
            'data': cursor.fetchmany(1000),
            'count': cursor.rowcount,
        })

    if req_data.get('backend') == 'mongo':
        # largely duplicate code from api_v1_queryExecuteCustom
        if req_data.get('query_name') is None:
            return jsonify({'error': 'query_name required'}), 400
        query_obj = queries['mongo'][req_data.get('query_name')]
        
        query = json.loads(query_obj.get('query'))
        collection = db['mongo'][query_obj.get('database')][query_obj.get('collection')]    
        cursor = collection.aggregate(query)
        
        obj = unbson(cursor)


        # kill it with fire
        return jsonify({
            'legend': list(obj[0].keys()),
            'data': [list(x.values()) for x in obj],
            'count': len(obj),
        })

    return jsonify({'error': 'Database is valid, but it has not been implemented in the frontend'}), 501

################################################################################

def isAdmin():
    return request.cookies.get('session') == '1'

queries = {
    'mysql': {


        'Get All Data FROM ODB': {'query':
'''SELECT *
FROM `ODB`.`health_data`'''},


        'Get All Data from ADB': {'query':
'''SELECT `state`, `statedim`.`name` as `state_name`, `period_start`, `period_end`, `cumulative`, `count`
FROM `ADB`.`fact`
INNER JOIN `ADB`.`statedim` ON `ADB`.`fact`.`id_state` = `ADB`.`statedim`.`id`
INNER JOIN `ADB`.`perioddim` ON `ADB`.`fact`.`id_period` = `ADB`.`perioddim`.`id`
INNER JOIN `ADB`.`datadim` ON `ADB`.`fact`.`id_data` = `ADB`.`datadim`.`id`;'''},


    'Get states with most cases (non-cumulative)': {'query':
'''SELECT `state`, SUM(`count`) as `count`, `cumulative`
FROM `ADB`.`fact`
INNER JOIN `ADB`.`statedim` ON `ADB`.`fact`.`id_state` = `ADB`.`statedim`.`id`
INNER JOIN `ADB`.`perioddim` ON `ADB`.`fact`.`id_period` = `ADB`.`perioddim`.`id`
INNER JOIN `ADB`.`datadim` ON `ADB`.`fact`.`id_data` = `ADB`.`datadim`.`id`
WHERE `cumulative` = 0
GROUP BY `state`, `cumulative`
ORDER BY `count` DESC;'''},
    },


    'mongo': {


        'Get All Data': {'database': 'valleyfever', 'collection': 'data', 'query': 
'''[
    {"$sort": {"index": 1}}
]'''},


        'Get total records per state': {'database': 'valleyfever', 'collection': 'data', 'query':
'''[
    {"$group": {"_id": "$state", "count": {"$sum": 1}}},
    {"$sort": {"count": -1}}
]'''},


        'Get state with most cases (non-cumulative)': {'database': 'valleyfever', 'collection': 'data', 'query':
'''[
    { "$match" : { "cumulative": 0}  },
    {"$group": {"_id": "$state", "sumNonCumulativeCount": { "$sum": "$count"}}},
    {"$sort": {"sumNonCumulativeCount" : -1}},
    {"$limit": 1}
]'''},
    },


    'neo4j': {


        'Graph of all records and what state they belong to': {'query': 
'''MATCH p=()-[r:IsInState]->() 
RETURN p LIMIT 500'''},


        'Graph of all records and what timeperiod they belong to': {'query':
'''MATCH p=()-[r:IsInPeriod]->() 
RETURN p LIMIT 1000
'''},


        'State and max number of cases in a timeperiod': {'query':
'''MATCH (n:Record) -   [r] - (m:State)
RETURN m.statename, max(n.count) , max(n.period_start), max(n.period_end)
ORDER BY max(n.count) DESC
'''},


        'Records with more than 500 infected, and what state it was in': {'query':
'''MATCH (n:Record) -   [r] - (m:State)
where n.count > 500
RETURN n,r,m
'''},


        'Max infected in a 1 week period for each state': {'query':
'''MATCH (n:Record) -   [r] - (m)
WHERE m.period_length = 6
RETURN n.state, max(n.count)
ORDER BY max(n.count) DESC
'''},

        'Max daily infection rates for each state': {'query':
'''MATCH (n:Record) -   [r] - (m)
RETURN n.state, max(n.count/m.period_length)
ORDER BY max(n.count/m.period_length) DESC
'''},
    },
}

app.jinja_env.globals['isAdmin'] = isAdmin
app.jinja_env.globals['queries'] = queries

def unbson(data):
    return json.loads(json_util.dumps(data))

################################################################################


if __name__ == '__main__':
    # Get config
    config = dotenv_values('../.env')

    # Init DB
    db['mysql'] = MySQLdb.connect(host='mysql', user=config.get('MYSQL_USER'), passwd=config.get('MYSQL_PASS'))
    db['mongo'] = MongoClient(host='mongo', port=int(config.get('MONGO_PORT')), username=config.get('MONGO_USER'), password=config.get('MONGO_PASS'))
    db['neo4j'] = GraphDatabase.driver('bolt://neo4j:%s' % config.get('NEO4J_PORT'), auth=(config.get('NEO4J_USER'), config.get('NEO4J_USER')))

    # Run Flask
    app.run(host='web', debug=True, port=5000)

    # Close DB
    for db_name in db:
        db[db_name].close()
