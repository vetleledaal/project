// GET  /api/v1/queryGet
async function queryGet(backend) {
    const response = await fetch('/api/v1/queryGet?' + new URLSearchParams({
        backend: backend,
    }));
    const data = await response.json();
    
    if (response.status != 200) {
        throw new Error(`${response.status}, ${data.error}`);
    }
    return data;
}

// POST /api/v1/queryExecuteCustom
async function queryExecuteCustom(backend, database, collection, query) {
    const response = await fetch('/api/v1/queryExecuteCustom', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            backend: backend,
            database: database,
            collection: collection,
            query: query,
        }),
    });
    const data = await response.json();
    
    if (response.status != 200) {
        throw new Error(`${response.status}, ${data.error}`);
    }
    return data;
}

// POST /api/v1/queryExecute
async function queryExecute(backend, database, collection, query_name) {
    const response = await fetch('/api/v1/queryExecute', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            backend: backend,
            database: database,
            collection: collection,
            query_name: query_name,
        }),
    });
    const data = await response.json();
    
    if (response.status != 200) {
        throw new Error(`${response.status}, ${data.error}`);
    }
    return data;
}

////////////////////////////////////////////////////////////////////////////////

async function updateQueries(queries) {
    document.querySelectorAll('section#queries > button:not(.dummy)').forEach(el => el.remove());
    for (const [name, query_obj] of Object.entries(queries)) {
        const button = document.createElement('button');
        button.innerText = name;
        button.addEventListener('click', async () => {
            document.querySelector('section#query > input[name="database"]').value = query_obj.database || 'N/A';
            document.querySelector('section#query > input[name="collection"]').value = query_obj.collection || 'N/A';
            document.querySelector('section#query > textarea[name="query"]').value = query_obj.query;
            document.querySelector('section#query > textarea[name="query"]').setAttribute('x-query-name', name);
        });
        document.querySelector('section#queries').appendChild(button);
    }
}


async function updateResults(results) {
    const table = document.getElementById('table');
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');

    const tr = document.createElement('tr');
    results['legend'].forEach(legend => {    
        const th = document.createElement('th');
        th.innerHTML = legend;
        tr.appendChild(th);
        
    });
    thead.appendChild(tr);

    results['data'].forEach(row => {
        const tr = document.createElement('tr');
        Object.keys(row).forEach(col => {
            const td = document.createElement('td');
            const obj = row[col];
            if (typeof obj == 'object') {
                td.innerText = JSON.stringify(obj);
            } else {
                td.innerText = obj;
            }
            tr.appendChild(td);
        });
        tbody.appendChild(tr);
    });
    table.replaceChildren(thead, tbody);
}

////////////////////////////////////////////////////////////////////////////////

document.querySelector('section#backend > select').addEventListener('change', async e => {
    const backend = e.target.value;
    const queries = await queryGet(backend);

    try {
        updateQueries(queries);
    } catch (e) {
        showError(e);
    }
});

document.querySelectorAll('section#query > input, section#query > textarea')
    .forEach(el => el.addEventListener('change',
        () => document.querySelector('section#query > textarea[name="query"]').removeAttribute('x-query-name')
    )
);


document.querySelector('section#query > button.execute').addEventListener('click', async () => {
    showError('');
    const backend = document.querySelector('section#backend > select').value;
    const database = document.querySelector('section#query > input[name="database"]').value;
    const collection = document.querySelector('section#query > input[name="collection"]').value;
    const query = document.querySelector('section#query > textarea[name="query"]').value;

    // Check if we can execute a preapproved query
    const query_name = document.querySelector('section#query > textarea[name="query"]').getAttribute('x-query-name');
    if(query_name === null) {
        // Execute custom query
        try {
            const data = await queryExecuteCustom(backend, database, collection, query);
            updateResults(data);
        } catch (e) {
            showError(e);
        }
    } else {
        // Execute preapproved query
        try {
            const data = await queryExecute(backend, database, collection, query_name);
            updateResults(data);
        } catch (e) {
            showError(e);
        }
    }
});




////////////////////////////////////////////////////////////////////////////////

function showError(e) {
    document.querySelector('section#results .error').innerText = e;
}