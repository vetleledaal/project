FROM docker.io/python:3.10.1
WORKDIR /app

COPY src/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY .env /

CMD ["python", "main.py"]
